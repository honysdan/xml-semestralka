# XML SEMESTRALKA

## Informace o projektu

### Zpracované oblasti
- Czechia: https://www.cia.gov/the-world-factbook/countries/czechia/
- United Kingdom: https://www.cia.gov/the-world-factbook/countries/united-kingdom/
- United States: https://www.cia.gov/the-world-factbook/countries/united-states/
- Canada: https://www.cia.gov/the-world-factbook/countries/canada/

### Navržení XML

Veškeré data obsažena v XML souborech byly převedeny ručně. Celková struktura pak byla inspirována strukturou, která je využívaná v Latex. Kořenovým elementem je element `country`, ten pak obsahuje další entity, které popisují jednotlivé informace o státu. `gallery` tento element funguje jako galerie, obsahuje elementy `image`, které obsahují odkazy na obrázky. Elementy `chapter` a `section` popisují hlavní kapitoly a sekce státu. Elementy `subsection`, `paragraph`, `list` a `data` již obsahují určité informace o státu.

#### Propojení XML souborů

Propojení XML souborů do jednoho je provedeno pomocí externích DTD entit. Dané propojení je popsáno v countries.xml, při jeho zpracování je pak vytvořen souhrnný xml soubor obsahující všechny státy.

### Validace pomocí DTD a RelaxNG

DTD schéma obsahuje popis jednotlivých entit, které se vyskytují v xml souborech. U RNG validačního schématu je využitá modularizace schématu (grammar), díky které je možné schéma využít i pro souhrnné xml.

### Generování HTML výstupů

HTML je generováno podle předepsané XSLT šablony, která je popsána v souboru "countries.xslt". Šablona obsahuje přesný popis, jak se má xml transformovat do html výstupu. K html výstupům je přidaný kaskádový styl popisující vzhled. Jednotlivé státy pak obsahují odpovídající obrázky a navigační lištu, která odkazuje na jednotlivé sekce. Dále je přidaný index, který odkazuje na jednotlivé státy.

### Generování PDF výstupu

PDF je generováno podle předepsané XSL šablony, která je popsána v souboru "countries.xsl". XSL soubor je nejdříve zpracovaný pomocí xmllint (XSL procesor), který vytvoří soubor s koncovkou .fo (formátovací objekt). Vytvořený soubor je pak zpracován fop (FO procesor), který vygeneruje finální PDF. Součástí finálního pdf jsou obrázky a obsah s odkazy na jednotlivé státy a sekce.

### Struktura projektu

`css` - adresář obsahující soubory popisující kaskádové styly

`resource` - adresář obsahující zdrojové XML soubory a dtd šablonu pro propojení

`stylesheet` - adresář obsahující xslt a xsl šablony pro generaci html (státy a index) a pdf

`validation` - adresář obsahující validační schémata (dtd a rng) pro validaci XML souborů

`output` - vygenerovaný adresář obsahující výstupy (html, pdf, xml)

## MAKEFILE

### Potřebný software

Mezi potřebný software patří make, xmllint a fop. Dále je potřebný saxon, který se musí nacházet ve stejném adresáři jako makefile.

### Makefile commands

`make all` - provede veškeré potřebné příkazy pro vytvoření finálního html a pdf výstupu (validizace všech XML souborů, generování podle šablon)

`make validate` - zvaliduje zdrojové XML soubory podle dtd a rng schématu

`make html` - vygeneruje html výstup

`make pdf` - vygeneruje pdf výstup

`make clear` - odstraní výstupní adresář

`make zip` - vytvoří zip s potřebnými soubory pro odevzdání
