<?xml version="1.0"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:my="myFunctions">

  <xsl:template match="/">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master master-name="content" page-height="297mm" page-width="210mm" margin="1in">
          <fo:region-body/>
        </fo:simple-page-master>
      </fo:layout-master-set>

      <fo:page-sequence master-reference="content">
        <xsl:call-template name="content"/>
      </fo:page-sequence>

    </fo:root>
  </xsl:template>

  <xsl:template name="content">
    <fo:flow flow-name="xsl-region-body">
      <xsl:apply-templates/>
    </fo:flow>
  </xsl:template>

  <xsl:template match="item">
    <fo:block-container padding-bottom="10mm">
      <fo:block>
        ID: <xsl:value-of select="@id"/>
      </fo:block>
      <xsl:apply-templates/>
    </fo:block-container>
  </xsl:template>

  <xsl:template match="title">
    <fo:block font-size="20pt" font-weight="bold" font-style="italic">
      <xsl:value-of select="."/>
    </fo:block>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="producer">
    <fo:block>
      PRODUCER: <fo:inline font-style="italic"><xsl:value-of select="."/></fo:inline>
    </fo:block>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="price">
    <fo:block>
      PRICE: <fo:inline font-style="italic"><xsl:value-of select="."/></fo:inline>
    </fo:block>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="type">
    <fo:block>
    TYPE: <fo:inline font-style="italic"><xsl:value-of select="@category"/></fo:inline>
    </fo:block>
    <xsl:apply-templates/>
  </xsl:template>

<!--
  item
    title
    producer
    price
    type

 -->
</xsl:stylesheet>
