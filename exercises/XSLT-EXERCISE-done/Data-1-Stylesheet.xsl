<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version="1.0">

<xsl:output method="html"/>

<xsl:template match="/">
    <html>
        <head>
            <title><xsl:value-of select="/article/articleinfo/title"/></title>
        </head>
        <body>
            <xsl:apply-templates/>
        </body>
    </html>
</xsl:template>

<xsl:template match="articleinfo">
  <div>
    <xsl:value-of select="."/>
  </div>
</xsl:template>

<xsl:template match="sect1">
  <div id="{@id}">
    <xsl:apply-templates/>
  </div>
</xsl:template>

<xsl:template match="sect1/title">
  <h1><xsl:value-of select="."/></h1>
</xsl:template>

<xsl:template match="sect1/sect2">
  <div>
      <xsl:apply-templates/>
  </div>
</xsl:template>

<xsl:template match="sect1/sect2/title">
  <h2><xsl:value-of select="."/></h2>
</xsl:template>

<xsl:template match="sect1/sect2/para | sect1/para">
  <p><xsl:value-of select="."/></p>
</xsl:template>

<xsl:template match="email">
  <b><xsl:value-of select="."/></b>
</xsl:template>

<xsl:template match="quote">
  <i><xsl:value-of select="."/></i>
</xsl:template>

<xsl:template match="ulink">
  <a href="@url"><xsl:value-of select="."/></a>
</xsl:template>

<xsl:template match="itemizedlist">
  <ul><xsl:apply-templates/></ul>
</xsl:template>

<xsl:template match="itemizedlist/listitem">
  <li><xsl:apply-templates/></li>
</xsl:template>

</xsl:stylesheet>
