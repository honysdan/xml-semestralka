COUNTRIES_XML = resource/canada.xml resource/czechia.xml resource/united-kingdom.xml resource/united-states.xml
OUTPUT_DIR = output

.PHONY: all
all: validation $(OUTPUT_DIR)/countries.xml html pdf

.PHONY: validation
validation: validation_dtd validation_rng

.PHONY: validation_dtd
validation_dtd:
	xmllint --noout --dtdvalid 'validation/countries.xml' $(COUNTRIES_XML)

.PHONY: validation_rng
validation_rng:
	xmllint --noout --relaxng 'validation/countries.rng' $(COUNTRIES_XML)

.PHONY: $(OUTPUT_DIR)/countries.xml
$(OUTPUT_DIR)/countries.xml:
	mkdir -p $(OUTPUT_DIR)
	xmllint --dropdtd --noent 'resource/countries.xml' > $@
	xmllint --noout --dtdvalid 'validation/countries.xml' $@
	xmllint --noout --relaxng 'validation/countries.rng' $@

.PHONY: html
html: $(OUTPUT_DIR)/countries.xml index
	mkdir -p $(OUTPUT_DIR)
	java -jar saxon.jar -s:$(OUTPUT_DIR)/countries.xml -xsl:'stylesheet/countries.xslt'

.PHONY: index
index:
	mkdir -p $(OUTPUT_DIR)
	java -jar saxon.jar -s:$(OUTPUT_DIR)/countries.xml -xsl:'stylesheet/index.xslt' -o:$(OUTPUT_DIR)/index.html

.PHONY: pdf
pdf: $(OUTPUT_DIR)/countries.xml
	mkdir -p $(OUTPUT_DIR)
	java -jar saxon.jar -s:$(OUTPUT_DIR)/countries.xml -xsl:'stylesheet/countries.xsl' -o:$(OUTPUT_DIR)/temp.fo
	fop -fo $(OUTPUT_DIR)/temp.fo -pdf $(OUTPUT_DIR)/countries.pdf
	rm $(OUTPUT_DIR)/temp.fo 2> /dev/null;

.PHONY: clear
clear:
	rm -rf $(OUTPUT_DIR) 2> /dev/null

.PHONY: zip
zip:
	mkdir -p $(OUTPUT_DIR)
	zip -r $(OUTPUT_DIR)/honysdan-xml.zip resource stylesheet validation Makefile README.md Zadání.md css
