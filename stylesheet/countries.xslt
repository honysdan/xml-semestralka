<?xml version="1.0"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:my="myFunctions">
  <xsl:output method="html"/>

  <xsl:template match="/countries">
    <xsl:for-each select="country">
      <xsl:variable name="name" select="@name"/>
      <xsl:result-document method="html" href="output/html/{$name}.html">
        <!-- process the country to its own html file -->
        <xsl:apply-templates select="."/>
      </xsl:result-document>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="country">
    <html lang="en">
      <head>
        <title><xsl:value-of select="@name"/></title>
        <link rel="stylesheet" href="../../css/country.css"/>
        <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@800&amp;family=Open+Sans&amp;display=swap" rel="stylesheet"/>
      </head>
      <body class="{@name}">
        <div class="content">
          <header>
            <h1><xsl:value-of select="@name"/></h1>
            <xsl:call-template name="navbar"/>
            <xsl:apply-templates select="gallery"/>
          </header>
          <xsl:apply-templates select="chapter"/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="navbar">
    <div class="navbar">
      <xsl:apply-templates select="chapter" mode="navbar"/>
    </div>
  </xsl:template>

  <xsl:template match="chapter" mode="navbar">
    <a href="#{@name}">
      <div class="navbar-button"><xsl:value-of select="@name"/></div>
    </a>
  </xsl:template>

  <xsl:template match="gallery">
    <div class="gallery">
      <xsl:for-each select="image">
        <a href="{@src}">
          <img src="{@src}"/>
        </a>
      </xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template match="country/chapter">
    <div class="chapter">
      <div class="chapter-title" id="{@name}">
        <h2><xsl:value-of select="@name"/></h2>
      </div>
      <xsl:apply-templates select="section"/>
    </div>
  </xsl:template>

  <xsl:template match="country/chapter/section">
    <div class="section">
      <div class="section-title">
        <h2>
          <xsl:value-of select="@name"/>
        </h2>
      </div>
      <div class="section-content">
        <xsl:value-of select="text()"/>
          <xsl:apply-templates select="*"/>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="paragraph">
    <p><xsl:value-of select="."/></p>
  </xsl:template>

  <xsl:template match="subsection">
    <p>
      <b>
        <xsl:if test="@name">
          <xsl:value-of select="@name"/>
          <xsl:text>: </xsl:text>
        </xsl:if>
      </b>
      <xsl:value-of select="my:commonAttributes(.)"/>
      <xsl:value-of select="text()"/>
    </p>
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="data">
    <p>
      <xsl:value-of select="my:commonAttributes(.)"/>
    </p>
  </xsl:template>

  <xsl:template match="list">
    <xsl:if test="@name">
      <h3>
        <xsl:value-of select="@name"/>
        <xsl:text>:</xsl:text>
      </h3>
    </xsl:if>

    <ul>
      <xsl:for-each select="item">
        <li>
          <xsl:if test="@name">
            <xsl:value-of select="@name"/>
            <xsl:text>: </xsl:text>
          </xsl:if>
          <xsl:value-of select="my:commonAttributes(.)"/>
        </li>
      </xsl:for-each>
    </ul>

    <xsl:if test="@note">
      <p>
        <xsl:text> (</xsl:text>
        <xsl:value-of select="@note"/>
        <xsl:text>)</xsl:text>
      </p>
    </xsl:if>
  </xsl:template>

  <xsl:function name="my:commonAttributes">
    <xsl:param name="node"/>
    <xsl:if test="$node/@value">
      <xsl:value-of select="$node/@value"/>
    </xsl:if>
    <xsl:if test="$node/@unit">
      <xsl:text> </xsl:text>
      <xsl:value-of select="$node/@unit"/>
    </xsl:if>
    <xsl:if test="$node/@note">
      <xsl:text> (</xsl:text>
      <xsl:value-of select="$node/@note"/>
      <xsl:text>)</xsl:text>
    </xsl:if>
  </xsl:function>

</xsl:stylesheet>
