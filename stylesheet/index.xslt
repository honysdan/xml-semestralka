<?xml version="1.0"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"/>

  <xsl:template match="/countries">
    <html lang="en">
      <head>
        <title>BI-XML - honysdan</title>
        <link rel="stylesheet" href="../css/index.css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans&amp;family=Roboto:ital,wght@0,400;1,700&amp;display=swap"/>
      </head>
      <body>
        <h1>BI-XML - honysdan</h1>
        <xsl:for-each select="country">
          <a href="./html/{@name}.html">
            <xsl:value-of select="@name"/>
          </a>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
