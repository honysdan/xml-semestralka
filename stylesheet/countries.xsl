<?xml version="1.0"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:my="myFunctions">

  <xsl:template match="/countries">
    <fo:root>

      <!-- LAYOUT DEFINITIONS -->
      <fo:layout-master-set>
        <!-- TITLE PAGE -->
        <fo:simple-page-master master-name="title-page" page-height="297mm" page-width="210mm">
          <fo:region-body margin-bottom="15mm" display-align="center"/>
        </fo:simple-page-master>
        <!-- CONTENT PAGE -->
        <fo:simple-page-master master-name="content" page-height="297mm" page-width="210mm" margin="0 1in 1in 1in">
          <fo:region-body margin="1in 0 0 0"/>
          <fo:region-before/>
          <fo:region-after/>
        </fo:simple-page-master>
        <!-- TABLE OF CONTENTS PAGE -->
        <fo:simple-page-master master-name="table-of-contents" page-height="297mm" page-width="210mm" margin="1in">
          <fo:region-body/>
          <fo:region-after/>
        </fo:simple-page-master>
      </fo:layout-master-set>

      <!-- TITLE PAGE - content -->
      <fo:page-sequence master-reference="title-page">
        <xsl:call-template name="title-page"/>
      </fo:page-sequence>
      <!-- TABLE OF CONTENTS PAGE - content -->
      <fo:page-sequence master-reference="table-of-contents">
        <xsl:call-template name="table-of-contents"/>
      </fo:page-sequence>
      <!-- CONTENT PAGE - content -->
      <fo:page-sequence master-reference="content">
        <xsl:call-template name="content"/>
      </fo:page-sequence>

    </fo:root>
  </xsl:template>

  <!-- TITLE PAGE TEMPLATE -->
  <xsl:template name="title-page">
    <fo:flow flow-name="xsl-region-body">

      <fo:block font-size="50pt" text-align="center" font-weight="bold" font-style="italic">
        BI-XML
      </fo:block>
      <fo:block text-align="center">
        honysdan
      </fo:block>

    </fo:flow>
  </xsl:template>

  <!-- TABLE OF CONTENTS PAGE TEMPLATE -->
  <xsl:template name="table-of-contents">
    <fo:flow flow-name="xsl-region-body">

      <fo:block font-size="20pt" text-align="center" font-weight="bold" font-style="italic">
        TABLE OF CONTENTS
      </fo:block>

      <!-- inspired by https://www.xmlpdf.com/tableofcontents.html -->
      <xsl:apply-templates select="country | country/chapter" mode="toc"/>

    </fo:flow>
  </xsl:template>

  <xsl:template match="country" mode="toc">
    <fo:block text-align='justify' text-align-last="justify" space-after="3pt" keep-with-next="always">
      <fo:basic-link>
          <xsl:variable name="country-number">
              <xsl:number level="multiple" count="country" format="1.1.1. "/>
          </xsl:variable>

          <xsl:variable name="reference">
              <xsl:number level="multiple" count="country" format="1.1"/>
          </xsl:variable>

          <xsl:attribute name='internal-destination'>chapter-refid-<xsl:value-of select="$reference"/></xsl:attribute>

          <xsl:value-of select="$country-number"/>
          <xsl:value-of select='@name'/>

          &#160;
          <fo:leader leader-pattern='dots' rule-thickness='.2pt'
                     leader-alignment='reference-area' font-size="10pt"/>
          &#160;

          <fo:page-number-citation>
              <xsl:attribute name='ref-id'>chapter-refid-<xsl:value-of select="$reference"/></xsl:attribute>
          </fo:page-number-citation>
      </fo:basic-link>
    </fo:block>
  </xsl:template>

  <xsl:template match="country/chapter" mode="toc">
    <fo:block text-align='justify' text-align-last="justify" space-after="3pt" margin-left="1cm"
          margin-right='.1cm'  font-size='0.9em'>

      <xsl:variable name="country-number">
          <xsl:number level="multiple" count="country | country/chapter" format="1.1. "/>
      </xsl:variable>

      <xsl:variable name="reference">
          <xsl:number level="multiple" count="country | country/chapter" format="1.1"/>
      </xsl:variable>
      <fo:basic-link>
        <xsl:attribute name='internal-destination'>chapter-refid-<xsl:value-of select="$reference"/></xsl:attribute>

        <xsl:value-of select='$country-number'/>
        <xsl:value-of select="@name"/>
        &#160;
        <fo:leader leader-pattern='dots' rule-thickness='.2pt'
                   leader-alignment='reference-area' font-size="10pt"/>
        &#160;

        <fo:page-number-citation>
          <xsl:attribute name='ref-id'>chapter-refid-<xsl:value-of select="$reference"/></xsl:attribute>
        </fo:page-number-citation>
      </fo:basic-link>
    </fo:block>
  </xsl:template>

  <!-- CONTENT PAGE TEMPLATE -->
  <xsl:template name="content">

    <!-- FOOTER -->
    <fo:static-content flow-name="xsl-region-after">
      <fo:block-container position="absolute" height="1in" width="100%">
        <fo:block text-align="center" margin-top="0.25in">
          <fo:page-number/>
        </fo:block>
      </fo:block-container>
    </fo:static-content>

    <!-- HEADER -->
    <fo:static-content flow-name="xsl-region-before">
      <fo:block-container position="absolute" height="1in" width="100%">
        <fo:block text-align-last="justify" margin-top="0.5in" color="gray">
          BI-XML
          <fo:leader leader-pattern="space"/>
          honysdan
        </fo:block>
      </fo:block-container>
    </fo:static-content>

    <!-- BODY -->
    <fo:flow flow-name="xsl-region-body">
      <xsl:apply-templates select="country"/>
    </fo:flow>

  </xsl:template>

  <xsl:template match="country">
    <xsl:variable name="reference">
      <xsl:number level="multiple" count="country | country/chapter" format="1.1"/>
    </xsl:variable>
    <!-- state name -->
    <fo:block font-size="50pt" text-align="center" font-weight="bold" font-style="italic" page-break-before="always">
      <xsl:attribute name='id'>chapter-refid-<xsl:value-of select="$reference"/></xsl:attribute>
      <xsl:value-of select="@name"/>
    </fo:block>
    <xsl:apply-templates select="gallery"/>
    <fo:block page-break-before="always">
      <xsl:apply-templates select="chapter"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="gallery">
    <fo:table width="100%" table-layout="fixed">
      <fo:table-body>
        <fo:table-row>
          <xsl:for-each select="image">
            <fo:table-cell display-align="center">
              <fo:block>
                <fo:external-graphic src="{@src}" content-height="scale-to-fit" width="100%" content-width="scale-to-fit" scaling="uniform"/>
              </fo:block>
            </fo:table-cell>
          </xsl:for-each>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>

  <xsl:template match="country/chapter">
    <xsl:variable name="reference">
      <xsl:number level="multiple" count="country | country/chapter" format="1.1"/>
    </xsl:variable>
    <fo:block font-size="30pt" margin-top="15pt">
      <xsl:attribute name='id'>chapter-refid-<xsl:value-of select="$reference"/></xsl:attribute>
      <xsl:value-of select="@name"/>
    </fo:block>
    <xsl:apply-templates select="section"/>
  </xsl:template>

  <xsl:template match="country/chapter/section">
    <xsl:variable name="dont1" select="'Economic overview'"/>
    <xsl:variable name="dont2" select="'Administrative divisions'"/>
    <xsl:choose>
      <xsl:when test="@name = $dont1">
        <fo:block-container>
          <xsl:call-template name="section-content"/>
        </fo:block-container>
      </xsl:when>
      <xsl:when test="@name = $dont2">
        <fo:block-container>
          <xsl:call-template name="section-content"/>
        </fo:block-container>
      </xsl:when>
      <xsl:otherwise>
        <fo:block-container keep-together.within-page="always">
          <xsl:call-template name="section-content"/>
        </fo:block-container>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="section-content">
    <fo:block font-size="20pt" font-weight="bold" color="gray" margin-bottom="5pt" margin-top="10pt">
      <xsl:value-of select="@name"/>
    </fo:block>
    <fo:block>
    <xsl:value-of select="text()"/>
    </fo:block>
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="paragraph">
    <fo:block text-indent="20pt" text-align="justify">
      <xsl:value-of select="."/>
    </fo:block>
  </xsl:template>

  <xsl:template match="data">
    <fo:block>
      <xsl:value-of select="my:commonAttributes(.)"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="list">
    <fo:block-container>
      <fo:block font-weight="bold">
        <xsl:if test="@name">
          <xsl:value-of select="@name"/>
          <xsl:text>:</xsl:text>
        </xsl:if>
      </fo:block>
      <fo:list-block>
        <xsl:for-each select="item">
          <fo:list-item>
            <fo:list-item-label end-indent="label-end()">
              <fo:block>&#x2022;</fo:block>
            </fo:list-item-label>
            <fo:list-item-body start-indent="body-start()">
              <fo:block>
                <xsl:if test="@name">
                  <xsl:value-of select="@name"/>
                  <xsl:text>: </xsl:text>
                </xsl:if>
                <xsl:value-of select="my:commonAttributes(.)"/>
                <xsl:if test="@note">
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="@note"/>
                    <xsl:text>)</xsl:text>
                </xsl:if>
              </fo:block>
            </fo:list-item-body>
          </fo:list-item>
        </xsl:for-each>
      </fo:list-block>
    </fo:block-container>
  </xsl:template>

  <xsl:template match="subsection">
    <fo:block-container keep-together.within-page="always">
      <fo:block font-weight="bold">
        <xsl:value-of select="@name"/>
        <xsl:text>: </xsl:text>
      </fo:block>
      <fo:block>
        <xsl:value-of select="my:commonAttributes(.)"/>
        <xsl:value-of select="text()"/>
      </fo:block>
      <xsl:apply-templates select="*"/>
    </fo:block-container>
  </xsl:template>

  <xsl:function name="my:commonAttributes">
    <xsl:param name="node"/>
    <xsl:if test="$node/@value">
      <xsl:value-of select="$node/@value"/>
    </xsl:if>
    <xsl:if test="$node/@unit">
      <xsl:text> </xsl:text>
      <xsl:value-of select="$node/@unit"/>
    </xsl:if>
    <xsl:if test="$node/@note">
      <xsl:text> (</xsl:text>
      <xsl:value-of select="$node/@note"/>
      <xsl:text>)</xsl:text>
    </xsl:if>
  </xsl:function>

</xsl:stylesheet>
