# ZADÁNÍ

## INSTRUKCE
- Vytvořte XML dokumenty se všemi daty pro každou vybranou oblast (z
  https://www.cia.gov/library/publications/the-world-factbook/). Podrobnosti
  k výběru oblastí jsou v souboru "BI-XML-PROJEKT-VÝBĚR-ZADÁNÍ*.txt". Pokud
  nezvládnete v dokumentu uvést všechna data o dané oblasti (např. protože
  se rozhodnete vytvářet XML manuálně), uveďte alespoň základní data pro
  minimálně 6 hlavních sekcí (tj. Introduction, Geography, atd.). Dále
  spojte XML dokumenty pro jednotlivé oblasti do jediného dokumentu pomocí
  DTD. `DONE`

- Vytvořte schema pomocí DTD i RelaxNG pro validaci struktury XML
  dokumentů. V RelaxNG schema se pokuste využít alespoň některé možnosti
  pokročilejší validace co jazyk nabízí navíc oproti DTD. `DONE`

- Pomocí XSLT vygenerujte (X)HTML výstupy (tj. souhrný index obsahující
  odkazy na jednotlivé oblasti a samostatnou stránku pro každou oblast
  obsahující detailní informace). `DONE`

- Pomocí XSL-FO vygenerujte PDF výstup (tj. dokument obsahující odkazy na
  jednotlivé oblasti a detailní informace o každé oblasti). `DONE`

- Buď vygenerujte ePub výstup (tj. ebook připravený k načtení v software
  podporujícím ePub formát), nebo do (X)HTML a PDF výstupů přidejte obrázky
  (stačí vlajky nebo mapy a podobně), navigaci pro sekce s detailními
  informacemi (stačí pro hlavní sekce jako "Introduction", "Geography",
  atd.) a rozšiřte základní formátování aby byly výstupy více přehledné
  (použijte CSS a možnosti formátování v XSL-FO včetně číslování stran a
  přidání záhlaví či zápatí). `DONE`

- Schema (DTD i RelaxNG), stylesheety (XSLT i XSL-FO) a další části
  projektu tvořte adekvátně obecné aby byla zajištěna funkčnost pro všechny
  zadané oblasti. Všechny XML dokumenty musí být "well-formed" + "valid". I
  základní výstupy bez rozšířeného formátování musí být adekvátně čitelné
  (tj. alespoň by mělo jít rozlišit nadpisy a odstavce, případně i další
  prvky jako seznamy, odkazy, atd.). `DONE`

- Projekt tvořte pomocí veřejně přístupného GIT repozitáře a finální
  soubory projektu (bez použitého software) zabalte do ZIP nebo TAR archivu
  a pošlete na email vyučujícího spolu s odkazem na GIT repozitář.

- Do archivu i repozitáře také přidejte informace o projektu dle zadání v
  souboru "BI-XML-PROJEKT-PREZENTACE*.txt".

## PREZENTACE
- Pokud děláte projekt samostatně do repozitáře přidejte také krátký popis
  projektu (adresářová struktura, potřebný software, seznam použitých
  příkazů (nebo skript s čitelným zdrojovým kódem co tyto příkazy provede),
  atd.).

  - 1) informace o navržené XML struktuře pro datové XML dokumenty a jejich
       propojení do jednoho XML dokumentu (můžete popsat i postup převodu
       dat do XML struktury)

  - 2) informace o validaci pomocí DTD a RelaxNG schema

  - 3) informace o generování (X)HTML výstupů

  - 4) informace o generování PDF výstupů

  - 5) informace o pokročilém formátování (X)HTML a PDF výstupů (pokud jste
       se rozhodli zpracovat tuto část)
